const toPersianDigits = digit => {
  const id = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
  return digit.replace(/[0-9]/g, function(w) {
    return id[+w];
  });
};

export default toPersianDigits;

import axios from 'axios';
import isEmpty from '../utils/is-empty';
import { SAVE_SELECT_OPTION, GET_ERRORS_CITY } from './types';

export const saveSelectValue = data => dispatch => {
  data.products = [];
  data.errors = [];

  if (!isEmpty(data.pcode)) {
    Promise.all(
      data.pcode.map((pcode, i) => {
        data.errors[i] = {};
        return Promise.all([
          axios
            .get(
              `${process.env
                .REACT_APP_API_URL}/cities/getCityFactSheetByCity/${pcode}`,
              {
                headers: {
                  ApiKey: process.env.REACT_APP_API_KEY
                }
              }
            )
            .catch(err => {
              data.errors[i].cityFactSheet =
                err.response.data.statusCode || null;
              return null;
            }),
          axios
            .get(
              `${process.env.REACT_APP_API_URL}/cities/${localStorage.jwtToken
                ? 'getCityProfileReportByCity'
                : 'getCityProfileReportSkeletonByCity'}/${pcode}`,
              {
                headers: {
                  ApiKey: process.env.REACT_APP_API_KEY
                }
              }
            )
            .catch(err => {
              data.errors[i].cityProfileReport =
                err.response.data.statusCode || null;
              return null;
            }),
          axios
            .get(
              `${process.env.REACT_APP_API_URL}/cities/${localStorage.jwtToken
                ? 'getInteractiveCityProfileByCity'
                : 'getInteractiveCityProfileSkeletonByCity'}/${pcode}`,
              {
                headers: {
                  ApiKey: process.env.REACT_APP_API_KEY
                }
              }
            )
            .catch(err => {
              data.errors[i].interactiveCityProfile =
                err.response.data.statusCode || null;
              return null;
            }),
          axios
            .get(
              `${process.env.REACT_APP_API_URL}/cities/${localStorage.jwtToken
                ? 'getDataSetsByCity'
                : 'getDataSetsSkeletonByCity'}/${pcode}`,
              {
                headers: {
                  ApiKey: process.env.REACT_APP_API_KEY
                }
              }
            )
            .catch(err => {
              data.errors[i].dataSets = err.response.data.statusCode || null;
              return null;
            }),
          axios
            .get(
              `${process.env
                .REACT_APP_API_URL}/cities/getUrbanBaselineByCity/${pcode}`,
              {
                headers: {
                  ApiKey: process.env.REACT_APP_API_KEY
                }
              }
            )
            .catch(err => {
              data.errors[i].urbanBaseline =
                err.response.data.statusCode || null;
              return null;
            })
        ]);
      })
    )
      .then(function(res) {
        if (!isEmpty(res)) {
          data.products = [];
          res.forEach((product, i) => {
            const cityFactSheet = product[0],
              cityProfileReport = product[1],
              interactiveCityProfile = product[2],
              dataSets = product[3],
              urbanBaseline = product[4];
            data.products[i] = {};
            data.products[i].cityFactSheet =
              cityFactSheet && cityFactSheet.data && cityFactSheet.data.en
                ? cityFactSheet.data
                : null;
            data.products[i].cityProfileReport =
              cityProfileReport && cityProfileReport.data === true
                ? true
                : cityProfileReport && cityProfileReport.data[0]
                  ? cityProfileReport.data[0].document.url
                  : null;
            data.products[i].interactiveCityProfile =
              interactiveCityProfile && interactiveCityProfile.data === true
                ? true
                : interactiveCityProfile && interactiveCityProfile.data[0]
                  ? interactiveCityProfile.data[0].name
                  : null;
            data.products[i].dataSets =
              dataSets && dataSets.data === true
                ? true
                : dataSets && dataSets.data[0]
                  ? dataSets.data[0].document.url
                  : null;
            data.products[i].urbanBaseline =
              urbanBaseline && urbanBaseline.data && urbanBaseline.data.en
                ? urbanBaseline.data
                : null;
          });
        }
        dispatch(setCurrentCityError(null));
        dispatch(setCurrentCityData(data));
      })
      .catch(function(err) {
        dispatch(
          setCurrentCityError(
            'Error! Something went wrong, please contact your web administrator'
          )
        );
        dispatch(
          setCurrentCityData({ name: [], pcode: [], products: [], errors: [] })
        );
        console.log(err);
      });
  }
  else {
    dispatch(
      setCurrentCityError(
        'No available UrbAN-S cities in this governorate, please check again soon'
      )
    );
    dispatch(setCurrentCityData(data));
  }
};

export const setCurrentCityData = decoded => {
  return {
    type: SAVE_SELECT_OPTION,
    payload: decoded
  };
};

export const setCurrentCityError = decoded => {
  return {
    type: GET_ERRORS_CITY,
    payload: decoded
  };
};

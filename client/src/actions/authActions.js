import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import Strapi from 'strapi-sdk-javascript';
import {
  GET_ERRORS_LOGIN,
  GET_ERRORS_REGISTER,
  SET_CURRENT_USER
} from './types';

const strapi = new Strapi(process.env.REACT_APP_API_URL);

// Register User
export const registerUser = userData => dispatch => {
  axios
    .post(`${process.env.REACT_APP_API_URL}/auth/local/register`, userData)
    .then(res => {
      dispatch({
        type: GET_ERRORS_REGISTER,
        payload: null //action.payload.response.data
      });
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS_REGISTER,
        payload: err.response.data //action.payload.response.data
      })
    );
};

// Login - Get User Token
export const loginUser = userData => dispatch => {
  axios
    .post(`${process.env.REACT_APP_API_URL}/auth/local`, userData)
    .then(res => {
      // Save to localStorage
      const token = `Bearer ${res.data.jwt}`;
      // Set token to ls
      localStorage.setItem('jwtToken', token);

      // Set token to Auth header
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);

      // Set current user
      strapi
        .getEntry('users', decoded.id)
        .then(res => {
          dispatch(setCurrentUser(res));

          // Redirect after login
          window.location.href = '/';
        })
        .catch(err => {
          //console.log(err);
          dispatch(setCurrentUser({}));
        });
      dispatch({
        type: GET_ERRORS_LOGIN,
        payload: null //action.payload.response.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS_LOGIN,
        payload: err.response.data
      });
      dispatch(setCurrentUser({}));
    });
};

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
  // Redirect after logout
  window.location.href = '/';
};

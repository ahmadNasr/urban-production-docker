import {
  GET_ERRORS_LOGIN,
  GET_ERRORS_REGISTER,
  GET_ERRORS_CITY
} from '../actions/types';

const initialState = {
  login: {},
  register: {},
  city: 'Choose a city, or click on a governorate to show available products'
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ERRORS_LOGIN:
      return {
        ...state,
        login: action.payload
      };
    case GET_ERRORS_REGISTER:
      return {
        ...state,
        register: action.payload
      };
    case GET_ERRORS_CITY:
      return {
        ...state,
        city: action.payload
      };
    default:
      return state;
  }
}

import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import {
  Container,
  MDBRow,
  MDBCol,
  MDBModal,
  MDBModalBody,
  toast,
  MDBBadge
} from 'mdbreact';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { show } from 'redux-modal';

const damageAssessmentMapSeries =
  'https://urban-syria.org/proxy/esri/storymap-series/damage-assessment';

class DamageAssessment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalDamage: false
    };
  }

  toggle = () => {
    this.setState({
      modalDamage: !this.state.modalDamage
    });
  };

  handleOpen = name => () => {
    toast.dismiss();
    this.props.show(name);
  };

  render() {
    const { t } = this.props;
    return (
      <Container
        fluid
        className={`${t('language.isRTL') && 'text-right'}`}
        dir={`${t('language.isRTL') && 'rtl'}`}>
        <MDBModal
          isOpen={this.state.modalDamage}
          toggle={this.toggle}
          size='fluid'
          centered
          className='dmg-assess px-4 modal-notify modal-info text-white text-justify font-weight-600'>
          <MDBModalBody>
            <iframe
              src={damageAssessmentMapSeries}
              frameBorder='0'
              allowFullScreen={true}
              width='100%'
              height='100%'
              title='Damage Assessment Map Series'
            />
          </MDBModalBody>
        </MDBModal>
        <MDBRow center className='urban-dark-orange z-depth-2'>
          <MDBCol lg='7' md='8' sm='9' size='10'>
            <div className='text-white my-5 mx-2'>
              <h2 className='h2-responsive text-center font-weight-bold pb-4 text-uppercase'>
                {t('damage-assessment.paragraph.title')}
                <MDBBadge color='urban-gray' className='mx-2 mb-2'>
                  {t('damage-assessment.paragraph.badge')}
                </MDBBadge>
              </h2>
              <p
                className={`font-weight-normal ${!t('language.isRTL') &&
                  'text-justify'}`}>
                {t('damage-assessment.paragraph.body.p1')}{' '}
                {localStorage.jwtToken ? (
                  <span>
                    {t('damage-assessment.paragraph.body.p5')}{' '}
                    <u
                      className='text-urban-elegant-gray-5 cursor-pointer'
                      onClick={this.toggle}>
                      {t('damage-assessment.paragraph.body.p6')}
                    </u>{' '}
                  </span>
                ) : (
                  <span>
                    {t('damage-assessment.paragraph.body.p2')}{' '}
                    <u
                      className='text-urban-elegant-gray-5 cursor-pointer'
                      onClick={this.handleOpen('authModal')}>
                      {t('damage-assessment.paragraph.body.p3')}
                    </u>{' '}
                    {t('damage-assessment.paragraph.body.p4')}
                  </span>
                )}
              </p>
            </div>
          </MDBCol>
        </MDBRow>
      </Container>
    );
  }
}

DamageAssessment.propTypes = {
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { show })(
  withTranslation()(DamageAssessment)
);

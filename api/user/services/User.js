'use strict';

/**
 * User.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const bcrypt = require('bcryptjs');

module.exports = {
  hashPassword: function(user = {}) {
    return new Promise(resolve => {
      if (!user.password || this.isHashed(user.password)) {
        resolve(null);
      } else {
        bcrypt.hash(`${user.password}`, 10, (err, hash) => {
          resolve(hash);
        });
      }
    });
  },

  isHashed: password => {
    if (typeof password !== 'string' || !password) {
      return false;
    }

    return password.split('$').length - 1 > 2;
  }
};

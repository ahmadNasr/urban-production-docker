module.exports = async (ctx, next) => {
  if (ctx.request && ctx.request.header && ctx.request.header.apikey) {
    const apiKeyConfig =
      process.env.NODE_ENV === 'production'
        ? strapi.config.environments.production.ApiKey
        : strapi.config.environments.development.ApiKey;
    if (ctx.request.header.apikey !== apiKeyConfig) return ctx.unauthorized();
  } else {
    ctx.unauthorized();
  }
  await next();
};
